class IllegalSize extends Error {
    constructor(message: string = "") {
        super(message)
    }
}

class IllegalToken extends Error {
    constructor(message: string = "") {
        super(message)
    }
}

class IllegalMove extends Error {
    constructor(message: string = "") {
        super(message)
    }
}

type Move = [number, number, number, number]

function fillArray<T>(length: number, elem: T): T[] {
    const array: T[] = []
    for (let i = 0; i < length; i++) array.push(elem)
    return array
}

function sumArray(array: number[]) {
    return array.length ? array.reduce((a, b) => a + b) : 0
}


function compareMove(moveA: Move, moveB: Move): boolean {
    return moveA[0] === moveB[0] && moveA[1] === moveB[1] && moveA[2] === moveB[2] && moveA[3] === moveB[3]
}


class Player {
    token: number = 1
    moves: Map<string, Move[]> = new Map<string, Move[]>()
    learnType: 0 | 1 | 2 | 3 = 0  // 0 None, 1 Reward, 2 Punish, 3 Both
    wins: number = 0

    constructor(token: number, learnType: 0 | 1 | 2 | 3 = 0) {
        this.token = token
        this.learnType = learnType
    }

    learn = (state: string, move: Move, situation: "win" | "loose" | 0 | 1) => {
        if (this.learnType === 0) return
        if ((situation === "win" || situation === 0) && (this.learnType === 1 || this.learnType === 3)) {
            if (this.moves.get(state) == undefined) this.moves.set(state, [])
            this.moves.set(state, [...(this.moves.get(state) as Move[]), move])
        }
        if ((situation === "loose" || situation === 1) && (this.learnType === 2 || this.learnType === 3)) {
            if (this.moves.get(state) == undefined) return
            if ((this.moves.get(state) as Move[]).length == 1) return
            this.moves.set(
                state,
                (this.moves.get(state) as Move[])
                    .filter((move_) => !compareMove(move_, move))
            )
        }
    }

    clone = (): Player => {
        const player: Player = new Player(this.token, this.learnType)
        player.wins = Number(this.wins)
        player.moves = new Map<string, Move[]>(this.moves)
        return player
    }

    toString = (): string => this.token.toString()

    isEqual = (other: Player): boolean => this.token === other.token
}

class Board {
    size: number = 3
    board: number[][] = []
    player1: Player // North to South
    player2: Player  // South to North

    constructor(size: number = 3, player1: Player = new Player(1), player2: Player = new Player(2)) {
        if (size < 3) throw new IllegalSize("Size must be 3 or more")
        if (player1.isEqual(player2)) throw new IllegalToken("Player tokens must be different")
        if (player1.token === 0 || player2.token === 0) throw new IllegalToken("Player tokens cannot be 0")

        this.size = size
        this.player1 = player1
        this.player2 = player2
        this.clear()
    }

    static fromState = (state: string): Board => {
        const stateArray: number[] = state.split("").map(Number)
        const size: number = Math.floor(Math.sqrt(stateArray.length))
        const tokens: number[] = stateArray.filter((c) => c !== 0)
        const player1Token: number = Math.min(...tokens)
        const player2Token: number = Math.max(...tokens)
        const board = new Board(size, new Player(player1Token), new Player(player2Token))
        stateArray.forEach((cell, i_) => {
            const i = Math.floor(i_ / size)
            const j = Math.floor(i_ % size)
            board.board[i][j] = cell
        })
        return board
    }

    isWin = (): Player | null => {
        const player1Clone = this.player1.clone()
        const player2Clone = this.player2.clone()

        if (this.board[this.board.length - 1].some((cell) => cell === this.player1.token)) return this.player1
        else if (this.board[0].some((cell) => cell === this.player2.token)) return this.player2
        else if (!([] as number[]).concat(...this.board).some((cell) => cell === this.player2.token)) return this.player1
        else if (!([] as number[]).concat(...this.board).some((cell) => cell === this.player1.token)) return this.player2
        else if (this.moves(player1Clone).length === 0 && this.moves(player2Clone).length === 0) return new Player(0)
        else if (this.moves(player1Clone).length === 0) return this.player2
        else if (this.moves(player2Clone).length === 0) return this.player1
        else return null
    }

    move = (x1: number, y1: number, x2: number, y2: number) => {
        if (this.board[y1][x1] === 0) throw new IllegalMove("Can only move players")
        if (this.board[y1][x1] === this.board[y2][x2]) throw new IllegalMove("Cannot move player onto itself")
        if (x1 < 0 || x1 >= this.size || y1 < 0 || y1 >= this.size || x2 < 0 || x2 >= this.size || y2 < 0 || y2 >= this.size) throw new IllegalMove("Out of bounds")
        if (Math.abs(x2 - x1) > 1 || Math.abs(y2 - y1) > 1) throw new IllegalMove("Can only move to adjacent spaces")
        if (this.board[y1][x1] === this.player1.token && y1 >= y2) throw new IllegalMove("Players can only move forward")
        if (this.board[y1][x1] === this.player2.token && y1 <= y2) throw new IllegalMove("Players can only move forward")

        this.board[y2][x2] = this.board[y1][x1]
        this.board[y1][x1] = 0
    }

    moves = (player: Player): Move[] => {
        const boardState: string = this.state()
        if (player.moves.get(boardState) != undefined) return player.moves.get(boardState) as Move[]

        const playerMoves: Move[] = []
        const direction: number = player.isEqual(this.player1) ? 1 : -1 // 1: North-South -1: South-North
        const other: Player = player.isEqual(this.player1) ? this.player2 : this.player1

        for (let y1 = 0; y1 < this.size; y1++) {
            const y2: number = y1 + direction
            if (y2 < 0 || y2 >= this.size) continue
            for (let x1 = 0; x1 < this.size; x1++) {
                if (this.board[y1][x1] !== player.token) continue
                if (this.board[y2][x1] === 0)
                    playerMoves.push([x1, y1, x1, y2]) // Forward
                if (x1 + 1 < this.size && this.board[y2][x1 + 1] === other.token)
                    playerMoves.push([x1, y1, x1 + 1, y2]) // Forward-East
                if (x1 > 0 && this.board[y2][x1 - 1] === other.token)
                    playerMoves.push([x1, y1, x1 - 1, y2]) // Forward-West
            }
        }

        player.moves.set(boardState, playerMoves)

        return player.moves.get(boardState) as Move[]
    }

    clear = () => {
        this.board = []
        this.board.push(fillArray(this.size, this.player1.token))
        for (let i = 0; i < this.size - 2; i++) this.board.push(fillArray(this.size, 0))
        this.board.push(fillArray(this.size, this.player2.token))
    }

    state = () => ([] as number[]).concat(...this.board).map(String).join("")

    toString = () => this.board.map(String).join("\n")
}

class Game {
    board: Board
    currentPlayer: Player
    turn: number = 0
    history: [number, string, Player, Move][] = []
    games: number = 0
    win: Player | null = null
    wins: number[] = []

    constructor(board: Board = new Board()) {
        this.board = board
        this.currentPlayer = this.board.player1
        this.newGame()
    }

    newGame = () => {
        this.turn = 0
        this.currentPlayer = this.board.player1
        this.history = []
        this.win = null
        this.board.clear()
    }

    private playMove = (move: Move, boardState = this.board.state()): Player | null => {
        if (this.win !== null) return null

        this.history.push([this.turn, boardState, this.currentPlayer, move])
        this.board.move(...move)

        let win: Player | null = this.board.isWin()

        if (win !== null) {
            win = win.token === 0 ? this.currentPlayer : win
            win.wins += 1
            this.games += 1
            this.wins.push(win.token)
            this.win = win
        } else {
            this.turn += 1
            this.currentPlayer = this.currentPlayer.isEqual(this.board.player1) ? this.board.player2 : this.board.player1
        }

        return win
    }

    play = (move: Move): Player | null => {
        if (this.board.board[move[0]][move[1]] !== this.currentPlayer.token) throw new IllegalMove("Can only move current player")

        const boardState: string = this.board.state()
        let moves: Move[] = []


        this.board.moves(this.currentPlayer)
        moves = this.board.moves(this.currentPlayer.clone())

        if (!moves.some((m) => compareMove(move, m))) throw new IllegalMove("Move not allowed")

        return this.playMove(move, boardState)
    }

    playAuto = (): Player | null => {
        const boardState: string = this.board.state()

        if (this.currentPlayer.moves.get(boardState) == undefined)
            this.board.moves(this.currentPlayer)

        const moves: Move[] = this.currentPlayer.moves.get(boardState) as Move[]
        if (moves.length === 0) throw new IllegalMove("No moves available")

        return this.playMove(moves[Math.floor(Math.random() * moves.length)], boardState)
    }

    playAutoGame = (): Player => {
        let win: Player | null = null
        while (win === null) win = this.playAuto()
        return win
    }

    playAutoGames = (times: number, learn: boolean = true): Player => {
        for (; times > 1; times--) {
            this.playAutoGame()
            if (learn) this.learn()
            this.newGame()
        }
        return this.playAutoGame()
    }

    learn = () => {
        if (this.win === null) return
        const lastHistoryPlayer1 = this.history
            .slice(this.history.length - 2)
            .filter((h) => h[2].isEqual(this.board.player1))
            .pop() as [number, string, Player, Move]
        const lastHistoryPlayer2 = this.history
            .slice(this.history.length - 2)
            .filter((h) => h[2].isEqual(this.board.player2))
            .pop() as [number, string, Player, Move]
        this.board.player1.learn(lastHistoryPlayer1[1], lastHistoryPlayer1[3], this.win.isEqual(this.board.player1) ? "win" : "loose")
        this.board.player2.learn(lastHistoryPlayer2[1], lastHistoryPlayer2[3], this.win.isEqual(this.board.player2) ? "win" : "loose")
    }

    toString = () => `${this.board.toString()}\n\n${this.turn} ${this.currentPlayer.toString()}`
}
