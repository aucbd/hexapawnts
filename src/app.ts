import express = require('express')
import path = require('path')
import {Express} from "express";

const app: Express = express()

const root: string = path.resolve(__dirname, "..")
const dist: string = path.resolve(root, "dist")
const docs: string = path.resolve(root, "docs")
const style: string = path.resolve(root, "style")
const assets: string = path.resolve(root, "assets")
const index: string = path.resolve(root, "index.html")

console.log('Server start')
console.log(`root: ${root}`)
console.log(`dist: ${dist}`)
console.log(`docs: ${docs}`)
console.log(`style: ${style}`)
console.log(`assets: ${assets}`)
console.log(`indexGame: ${index}`)

app.get("/", (req, res) =>
    res.sendFile(index))

app.use("/docs", express.static(docs))
app.use("/dist", express.static(dist))
app.use("/style", express.static(style))
app.use("/assets", express.static(assets))

app.listen(process.env.PORT || 8080)
