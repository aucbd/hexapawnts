function clearChildren(elem: HTMLElement, skip: number = 0) {
    while (elem.childElementCount > skip)
        elem.removeChild(elem.childNodes[elem.childElementCount - 1])
}

function makeBoard(size: number, tableElem: HTMLTableElement) {
    clearChildren(tableElem)
    for (let i = 0; i < size; i++) {
        const tr: HTMLTableRowElement = document.createElement("tr")
        tableElem.appendChild(tr)
        for (let j = 0; j < size; j++) {
            tr.appendChild(document.createElement("td"))
        }
    }
}

function drawBoard(board: number[][], tableElem: HTMLTableElement) {
    board.forEach((row, i) => {
        row.forEach((cell, j) => {
            const cellElement: HTMLTableCellElement = tableElem.rows[i].cells[j]
            if (cell === 1) {
                cellElement.classList.remove("white")
                cellElement.classList.add("black")
            } else if (cell === 2) {
                cellElement.classList.remove("black")
                cellElement.classList.add("white")
            } else {
                cellElement.classList.remove("white")
                cellElement.classList.remove("black")
            }
        })
    })
}

function fillBoard(board: (number | string)[][], tableElem: HTMLTableElement) {
    board.forEach((row, i) => {
        row.forEach((cell, j) => {
            tableElem.rows[i].cells[j].innerHTML = cell.toString()
        })
    })
}

function drawMoves(player: Player, tableElem: HTMLTableElement) {
    clearChildren(tableElem)
    const trHead: HTMLTableRowElement = document.createElement("tr")
    const th: HTMLTableHeaderCellElement = document.createElement("th")
    th.colSpan = 2
    th.innerText = `Player ${player.token} Moves`
    trHead.appendChild(th)
    tableElem.append(trHead)

    Array.from(player.moves.entries())
        .forEach(([state, moves]) => {
            const board: Board = Board.fromState(state)

            const tr: HTMLTableRowElement = document.createElement("tr")
            const tdState: HTMLTableDataCellElement = document.createElement("td")
            const tdMoves: HTMLTableDataCellElement = document.createElement("td")
            const tableBoard: HTMLTableElement = document.createElement("table")

            tableBoard.classList.add("board")
            tableBoard.classList.add("small")
            tableBoard.classList.add("fontSmall")
            tableBoard.classList.add("borders")
            makeBoard(board.size, tableBoard)
            drawBoard(board.board, tableBoard)

            tdMoves.innerHTML = moves
                .slice(0, moves.length > state.length ? state.length : moves.length)
                .map(([x1, y1, x2, y2]) => `${x1},${y1} &rarr; ${x2},${y2}`)
                .join("</br>")
            if (moves.length > state.length) tdMoves.innerHTML += `</br>&vellip;</br>${moves.length - state.length}`
            tdMoves.style.width = "fit-content"

            tdState.appendChild(tableBoard)
            tr.appendChild(tdState)
            tr.appendChild(tdMoves)
            tableElem.appendChild(tr)
        })
}

function drawHistory(history: [number, string, Player, Move][], tableElem: HTMLTableElement) {
    clearChildren(tableElem)
    const trHead: HTMLTableRowElement = document.createElement("tr")
    const th: HTMLTableHeaderCellElement = document.createElement("th")
    th.colSpan = 4
    th.innerText = "History"
    trHead.appendChild(th)
    tableElem.append(trHead)

    history
        .sort(([turnA, ..._], [turnB, ...__]) => turnA < turnB ? -1 : turnA === turnB ? 0 : 1)
        .forEach(([turn, state, player, [x1, y1, x2, y2]]) => {
            const tr: HTMLTableRowElement = document.createElement("tr")
            const tdTurn: HTMLTableDataCellElement = document.createElement("td")
            const tdPlayer: HTMLTableDataCellElement = document.createElement("td")
            const tdState: HTMLTableDataCellElement = document.createElement("td")
            const tdMove: HTMLTableDataCellElement = document.createElement("td")
            const tableBoard: HTMLTableElement = document.createElement("table")

            tdTurn.innerText = turn.toString()
            tdTurn.style.textAlign = "center"
            tdPlayer.style.width = "30px"

            tdPlayer.innerText = player.token.toString()
            tdPlayer.style.textAlign = "center"
            tdPlayer.style.width = "30px"

            tableBoard.classList.add("board")
            tableBoard.classList.add("small")
            tableBoard.classList.add("borders")
            const board = Board.fromState(state)
            makeBoard(board.size, tableBoard)
            drawBoard(board.board, tableBoard)

            tdMove.innerHTML = x1 !== undefined ? `${x1},${y1} &rarr; ${x2},${y2}` : ""
            tdMove.style.textAlign = "center"

            tdState.appendChild(tableBoard)
            tr.appendChild(tdTurn)
            tr.appendChild(tdPlayer)
            tr.appendChild(tdState)
            tr.appendChild(tdMove)
            tableElem.appendChild(tr)
        })
}

function reloadReset(): void {
    const url = new URL(window.location.href)
    for (const key of Array.from(url.searchParams.keys())) url.searchParams.delete(key)
    window.location.href = url.href
}

function exceptionCatch<R, S>(fn: (...args: []) => R, fnFail: (...args2: []) => S): R | S | void {
    try {
        return fn()
    } catch (err_) {
        const err: Error = err_ as Error
        console.log(err)
        window.alert(`${err.name}: ${err.message}`)
        if (fnFail !== undefined) return fnFail()
    }
}
